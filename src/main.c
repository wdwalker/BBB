#include "LPC17xx.h"

void delay_ms(unsigned int count);
int main(void);

/* system_LPC17xx.c sets up the clock as so:
 *
 * CLKSRCSEL_Val 0x00000001: use the external oscillator (FIN=12MHz)
 * PLL0CFG_Val   0x0000000B: N = 1, M = 12
 * CCLKCFG_Val   0x00000002: CCLK = 3;
 *
 * From p43 of "UM10360 - LPC176x/5x User Manual" at
 * http://www.nxp.com/documents/user_manual/UM10360.pdf
 * we then see:
 *
 * FCCO = (2*M*FIN)/N
 *      = (2*12*12000000)/1 = 288000000
 *
 * Finally, to get the frequency of the CPU clock, we divide by CCLK
 * to get a frequency of 96000000, or 96MHz. So, we'll roughly call
 * a loop of 9600 iterations the equivalent of 1ms.
 *
 * Or....system_LPC17xx.c also puts this core clock value in
 * SystemCoreClock so just divide by 10000 to turn MHz into
 * ticks-per-ms. This way, we're not so sensitive to changes in
 * the clock configuration done in system_LPC17xx.c.
 */
uint32_t ticks_per_ms;
void delay_ms(unsigned int count) {
	volatile unsigned int j = 0, i = 0;
	for (j = 0; j < count; j++) {
		for (i = 0; i < ticks_per_ms; i++) {
		};
	}
}

/* http://anhnvnguyen.blogspot.com/2010/04/lpc17xx-gpio-basic_05.html
 * has a good write up of selecting pins. The following correspond to
 * LED1-4 on the NXP LPC1768 mbed development board.
 *
 * See also p119 of "UM10360 - LPC176x/5x User Manual" as well as
 * other pages referenced below.
 */
#define P1_18_SEL (1 << 5) | (1 << 4)   /* Pin 1.18 = LED1 */
#define P1_20_SEL (1 << 9) | (1 << 8)   /* Pin 1.20 = LED2 */ 
#define P1_21_SEL (1 << 11) | (1 << 10) /* Pin 1.21 = LED3 */
#define P1_23_SEL (1 << 15) | (1 << 14) /* Pin 1.23 = LED4 */

#define P18 (1 << 18)
#define P20 (1 << 20)
#define P21 (1 << 21)
#define P23 (1 << 23)

#define ALL (P18 | P20 | P21 | P23)

int main(void) {
	/* How many core clock ticks in 1ms.
	 */
	ticks_per_ms = SystemCoreClock / 10000;

	/* Set the pins to GPIO by setting the associated bits to 00 (p119)
	 */
	LPC_PINCON->PINSEL3 &= (unsigned) ~(P1_18_SEL | P1_20_SEL | P1_21_SEL
			| P1_23_SEL);

	/* Set the direction as output by setting the associated bits
	 * to 1 (p133)
	 */
	LPC_GPIO1->FIODIR |= ALL;

	/* Turn the LEDs on and off at roughly 1Hz.
	 */
	while (1) {
		LPC_GPIO1->FIOSET |= ALL; /* p132 */
		delay_ms(500);
		LPC_GPIO1->FIOCLR |= ALL; /* p132 */
		delay_ms(500);
	}
}
