# BBB

## Overview

The purpose of the Bare Bones Blinky (BBB) is mostly as a learning
tool for bare bones ARM Cortex development on Mac OS X using the GNU
ARM Embedded Toolchain.

The source code in this repository will compile and deploy cleanly
from the command line. No IDE is required. There are two ways to build
the blinky - these build the same exact main.c and are provided as a
means to compare the two environments.

* makefile (run from the src directory) - as bare as you can get with C
* mbed (run from the top level directory) - as bare as you can get
  with [mbed](https://www.mbed.com/en/platform/mbed-os/) (see also
  "[Introducing mbed OS 5](https://developer.mbed.org/blog/entry/Introducing-mbed-OS-5/)").


Furthermore, debugging can be performed using OpenOCD (see instructions below).

The code is written for the NXP LPC1768 mbed development board. The
NXP LPC1768 mbed development board was chosen primarily because of the
ease of flashing applications via the USB mass storage
interface. Note, however, that mbed is not used to build the
application.

The LPC1768 board provides the following in a very simple package that
can be powered and managed via a USB interface:

* ARM Cortex-M3 Core
* 96MHz
* 32KB RAM, 512KB FLASH
* Ethernet
* USB Host/Device
* 2xSPI
* 2xI2C
* 3xUART
* CAN
* 6xPWM
* 6xADC
* GPIO

More information can also be found at the following:

* [mbed-LPC1768](https://developer.mbed.org/platforms/mbed-LPC1768/)
* [LPC1768 Reference Design](https://developer.mbed.org/media/uploads/chris/lpc1768-refdesign-schematic.pdf)
* [LPC1768 Data Sheet](http://www.nxp.com/documents/data_sheet/LPC1769_68_67_66_65_64_63.pdf)
* [LPC1768 User Manual](http://www.nxp.com/documents/user_manual/UM10360.pdf)

## Source Code

Under the BBB directory, you will find several files. The intent here
is to be true to the original sources rather than just copy them. To
that end, diff files are used so you can tell exactly what is
different from the original sources.

* Files used by both the makefile and mbed builds
   * ./src/main.c
      * The simplest file of all.
   * ./src/gdb-mbed-lpc1768.cfg
      * OpenOCD configuration file for debugging (see below).

* Files specific to the makefile build:
  * ./CMSIS_5
     * Git submodule clone of
   [Cortex Microcontroller Software Interface Standard, Version 5](http://github.com/ARM-software/CMSIS_5.git). This
   is used to for include files as well as the basis for several other
   sources.
  * ./src/LPC1768.ld.diff
     * Diffs from ../CMSIS\_5/Device/ARM/ARMCM3/Source/GCC/gcc_arm.ld to set the MEMORY section for the LPC1768.
  * ./src/LPC17xx.h.diff
     * Diffs from ARMmbed mbed-os
   [LPC17xx.h](https://raw.githubusercontent.com/ARMmbed/mbed-os/master/targets/TARGET_NXP/TARGET_LPC176X/device/LPC17xx.h) to fix compiler warnings.
 * ./src/system_LPC17xx.c.diff
     * Diffs from ARMmbed mbed-os
       [system_LPC17xx.c](https://raw.githubusercontent.com/ARMmbed/mbed-os/master/targets/TARGET_NXP/TARGET_LPC176X/device/system_LPC17xx.c) to fix compiler warnings.
 * ./src/makefile
     * Builds and deploys; also curls sources from the net and applies the diff files.

* Files specific to the mbed build
  * ./mbed-os.lib
      * When doing an 'mbed import', the mbed CLI uses this file to
        determine which version of
        [mbed-os](https://github.com/ARMmbed/mbed-os) to grab.
  * ./mbed_app.json
      * When doing an 'mbed compile', the mbed CLI reads this for app
        specific settings; this file strips out many things not needed
        by the blinky.
  * /.mbedignore
      * When doing an 'mbed compile', this file tells the mbed CLI to
        omit files; the contents of this file strip out all the
        features we don't care about, including the mbed OS itself.

## Build Environment

### GNU Arm Embedded Toolchain
The development environment for BBB is a Mac OS Sierra system with the
[GNU ARM Embedded Toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads). The
version used is gcc-arm-none-eabi-6-2017-q1-update from 23-Feb-2017.

> NOTE: do not use gcc-arm-none-eabi-6_2-2016q4. It has a bug in gdb
> that causes it to crash with an "Abort trap: 6".

To build, you need ..../gcc-arm-none-eabi-6-2017-q1-update/bin in your
PATH (the exact path will depend upon where you installed it).

### mbed OS 5
For the mbed build, you need the mbed OS 5 CLI installed:

```
git clone git@github.com:ARMmbed/mbed-cli.git
cd mbed-cli
python setup.py install
```

## Makefile Building
### Source Code

`git clone --recursive git@github.com:wdwalker/BBB.git`

### Building and Deploying

```
cd BBB/src
make clean all deploy
```

This will create a `BBB.bin` file and copy it to `/Volumes/MBED`,
which will flash it to your LPC1768.

If you see the following compilation error, it's because you didn't do
a recursive clone (see above):

> LPC17xx.h:99:101: fatal error: core\_cm3.h: No such file or directory</br>
> \#include "core\_cm3.h" /* Cortex-M3 processor and core peripherals */

Once you have done a successful `make clean all deploy`, you can press
the button on the LPC1768 to reset it and then watch all four of its
LEDs blink over and over.

### Debug Build

The debug build is simple - all it does is add some debug flags to the
compile and link in the rdimon library to enable debugging on the
LPC1768 (aka
[semihosting](http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0471g/Bgbjjgij.html)).

```
make clean all deploy DEBUG=1
```

When you press the reset button on the LPC1768, the blinky app will
still run - you should see the LEDs blinking. We will stop that soon
enough with the debugger.

## mbed Building
### Source Code
```mbed import git@github.com:wdwalker/BBB```

### Building and Deploying

```
cd BBB
mbed compile -c -m LPC1768 -t GCC_ARM
cp ./BUILD/LPC1768/GCC_ARM/BBB.bin /Volumes/MBED
```

### Debug Build

```
cd BBB
mbed compile -c -m LPC1768 -t GCC_ARM --profile mbed-os/tools/profiles/debug.json
cp ./BUILD/LPC1768/GCC_ARM/BBB.bin /Volumes/MBED
```

## Debugging

Debugging is identical for the makefile and mnbed versions.

### Dependencies

Debugging has two dependencies: CMSIS-DAP on your LPC1768 and OpenOCD
on your Mac.

#### CMSIS-DAP

The [CMSIS-DAP](https://developer.mbed.org/handbook/CMSIS-DAP) debug
interface consists of an abstraction of the Cortex Debug Access Port
(DAP) command set over a driver-less USB HID connection. In English,
this means you can debug the LPC1768 without any extra hardware.

In order to get CMSIS-DAP on your LPC1768, you need to update the
firmware. The
[mbed site](https://developer.mbed.org/handbook/Firmware-LPC1768-LPC11U24)
has firmware rev 141212 for the LPC1768 that contains
[CMSIS-DAP](https://developer.mbed.org/handbook/CMSIS-DAP) support.

#### OpenOCD

[OpenOCD](http://openocd.org) is the Open On-Chip Debugger. For our
purposes, it runs as a process on your Mac that talks to the CMSIS-DAP
on the LPC1768 one side, and your gcc-arm-none-eabi-gdb session on the
other.

You can install OpenOCD with homebrew:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install openocd
```

### Attaching OpenOCD to the LPC1768

After deploying a debug build to the LPC1768 (remember to press the
reset button!), you then need to attach OpenOCD to it. This is quite
simple and it uses `src/gdb-mbed-lpc1768.cfg`.

```
openocd -f src/gdb-mbed-lpc1768.cfg
```

You should see output similar to the following, showing that OpenOCD
has found the LC1768. Note that OpenOCD is still young and can be
quirky. Sometimes you need to start it more than once to connect to
the board.

```
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
	http://openocd.org/doc/doxygen/bugs.html
Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
adapter speed: 10 kHz
adapter_nsrst_delay: 200
cortex_m reset_config sysresetreq
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : CMSIS-DAP: FW Version = 1.0
Info : SWCLK/TCK = 0 SWDIO/TMS = 1 TDI = 0 TDO = 0 nTRST = 0 nRESET = 1
Info : CMSIS-DAP: Interface ready
Info : clock speed 10 kHz
Info : SWD DPIDR 0x2ba01477
Info : lpc17xx.cpu: hardware has 6 breakpoints, 4 watchpoints
```

### Running arm-none-eabi-gdb

This can be a finicky process. The LPC1768 doesn't always want to stop
and you may need to restart OpenOCD before starting arm-none-eabi-gdb.

To run arm-none-eabi-gdb, enter the following in a terminal
window. This tells gdb to connect to OpenOCD (which is listening on
port 3333 for commands from gdb) and then tell OpenOCD to reset and
halt the LPC1768.

> For the makefile version, point to ```./Debug/BBB.elf```. For the mbed version, point to ```BUILD/LPC1768/GCC_ARM/BBB.elf```.

```
arm-none-eabi-gdb -ex "set remotetimeout 60"  -ex "target remote :3333" -ex "monitor reset halt" BBB.elf
```

You should see something like this in the gdb window:

```
arm-none-eabi-gdb -ex "set remotetimeout 60"  -ex "target remote :3333" -ex "monitor reset halt" BBB.elf
GNU gdb (GNU Tools for ARM Embedded Processors 6-2017-q1-update) 7.12.1.20170215-git
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=x86_64-apple-darwin10 --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from BBB.elf...done.
Remote debugging using :3333
0x1fff0080 in ?? ()
keep_alive() was not invoked in the 1000ms timelimit. GDB alive packet not sent! (1076). Workaround: increase "set remotetimeout" in GDB
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x1fff0080 msp: 0x10001ffc
(gdb)
```

You should also see something like this in the OpenOCD window:

```
Info : accepting 'gdb' connection on tcp/3333
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x1fff0080 msp: 0x10001ffc
Warn : keep_alive() was not invoked in the 1000ms timelimit. GDB alive packet not sent! (1078). Workaround: increase "set remotetimeout" in GDB
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x1fff0080 msp: 0x10001ffc
```

Now, debug away:

```
handle SIGTRAP nostop
break main.c:main
cont
```

Suhweet! No IDE. All from the command line. Simple.

## More fun with OpenOCD

Telnet into OpenOCD and you can
[talk to it directly](http://openocd.org/doc/html/General-Commands.html):

```
telnet localhost 4444
```
